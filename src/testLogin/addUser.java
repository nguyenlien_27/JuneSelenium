package testLogin;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObject.addnewuserObject;
import PageObject.allusersObject;
import PageObject.dashboardObject;
import PageObject.loginObject;
import common.utils;

public class addUser {

	@Before
	public void before() {
		System.getProperty("webdriver.gecko.driver", "geckodriver.exe");
		utils.driver = new FirefoxDriver();
	}

	@Test
	public void addUserAndCheck() throws InterruptedException {
		//login
		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(8000);
		String VerifyTextLoginSucessfull = utils.driver.findElement(dashboardObject.verifyLoginSuccessfull).getText();
		Assert.assertEquals("Dashboard", VerifyTextLoginSucessfull);
	
		// load add all user page
		utils.driver.findElement(allusersObject.mnuAlluser).click();
		Thread.sleep(3000);

		// check user is exist or not
		utils.driver.findElement(By.id("user-search-input")).sendKeys("user01");
		utils.driver.findElement(By.id("search-submit")).click();
		Thread.sleep(3000);
		
		//Verify text on the page
		if(utils.driver.getPageSource().contains("No matching users were found.")){
			//create user
			utils.driver.findElement(addnewuserObject.mnuAddnewuser).click();
			Thread.sleep(3000);
			utils.driver.findElement(addnewuserObject.txtUsername).sendKeys("user01");
			utils.driver.findElement(addnewuserObject.txtEmail).sendKeys("nguyenlien.7@gmail.com");
			utils.driver.findElement(addnewuserObject.txtPass1).sendKeys("123456");
			utils.driver.findElement(addnewuserObject.txtPass2).sendKeys("123456");
			utils.driver.findElement(addnewuserObject.btnCreatuser).click();
			Thread.sleep(5000);
		} else {
			//del the user
			utils.driver.findElement(allusersObject.chkAlluser).click();
			utils.driver.findElement(allusersObject.slbAction).sendKeys("Delete");
			Thread.sleep(3000);
			utils.driver.findElement(allusersObject.btnDoaction).click();
			Thread.sleep(5000);
			utils.driver.findElement(allusersObject.rdoDeleteoption).click();
			utils.driver.findElement(allusersObject.btnDeleteuser).click();
			Thread.sleep(5000);
		}
			//go to add new user screen then input info for new user
			utils.driver.findElement(addnewuserObject.mnuAddnewuser).click();
			Thread.sleep(3000);
			utils.driver.findElement(By.id("user_login")).sendKeys("user01");
			utils.driver.findElement(By.id("email")).sendKeys("nguyenlien.7@gmail.com");
			utils.driver.findElement(By.id("pass1")).sendKeys("123456");
			utils.driver.findElement(By.id("pass2")).sendKeys("123456");
			utils.driver.findElement(By.id("createusersub")).click();
			Thread.sleep(5000);
			
	}
	@After
	public void after(){
	//	utils.driver.quit();	
	}
}