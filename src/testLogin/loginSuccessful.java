package testLogin;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import PageObject.dashboardObject;
import PageObject.loginObject;
import common.utils;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
 

public class loginSuccessful {

	@Before
	public void before() {
		System.setProperty("webdriver.gecko.dirver", utils.Geckodriver);
		utils.driver = new FirefoxDriver();
	}

	@Test
	public void login() throws InterruptedException, IOException {
		//login
		utils.driver.get(utils.URL);
		utils.driver.findElement(loginObject.txtUsername).sendKeys(utils.Username);
		utils.driver.findElement(loginObject.txtPassword).sendKeys(utils.Password);
		utils.driver.findElement(loginObject.btnSubmit).click();
		Thread.sleep(8000);
		String VerifyTextLoginSucessfull = utils.driver.findElement(dashboardObject.verifyLoginSuccessfull).getText();
		Assert.assertEquals("Dashboard", VerifyTextLoginSucessfull);
		
		//get current date time
		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
		//get current date time with Date()
		Date date = new Date();
		 
		 // Now format the date
		 String currentdatetime = dateFormat.format(date);
		 System.out.println(currentdatetime);
		
		//take screen short
		File src= ((TakesScreenshot)utils.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/"+"dashboard_"+currentdatetime+".png"));

		
		System.out.println("Congratulations!! Pass all");
	}

	@After
	public void after() throws Exception {
		utils.driver.quit();
	}
}
