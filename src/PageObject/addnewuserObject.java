package PageObject;

import org.openqa.selenium.By;

public class addnewuserObject {
	//menu add new user
	public static By mnuAddnewuser = By.xpath(".//*[@id='menu-users']/ul/li[3]/a");
	
	//user name
	public static By txtUsername = By.id("user_login");
	
	//email
	public static By txtEmail = By.id("email");
	
	//pass
	public static By txtPass1 = By.id("pass1");
	
	//confirm pass
	public static By txtPass2 = By.id("pass2");
	
	//add new user button
	public static By btnCreatuser = By.id("createusersub");
}
