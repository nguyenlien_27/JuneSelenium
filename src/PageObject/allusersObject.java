package PageObject;

import org.openqa.selenium.By;

public class allusersObject {
	//menu all users
	public static By mnuAlluser = By.xpath(".//*[@id='menu-users']/a/div[3]");
	
	//search user text box
	public static By txtSearchuser = By.id("user-search-input");
	
	//search user button
	public static By btnSearchuser = By.id("search-submit");
	
	//check all user on list
	public static By chkAlluser = By.xpath(".//*[@id='cb-select-all-1']");
	
	//action
	public static By slbAction = By.id("bulk-action-selector-top");
	
	//apply button
	public static By btnDoaction = By.id("doaction");
	
	//confirm delete all content
	public static By rdoDeleteoption = By.xpath(".//*[@id='delete_option0']");
	
	//confirm deletion button
	public static By btnDeleteuser = By.xpath(".//*[@id='submit']");
}
