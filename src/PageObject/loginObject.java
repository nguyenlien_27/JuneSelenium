package PageObject;

import org.openqa.selenium.By;

public class loginObject {
	//user name text box
	public static By txtUsername = By.xpath(".//*[@id='user_login']");
	
	//pass word
	public static By txtPassword = By.xpath(".//*[@id='user_pass']");
	
	//log in button
	public static By btnSubmit = By.xpath(".//*[@id='wp-submit']");    

}
